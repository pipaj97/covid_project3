# compute multiple sequence alignment
rule mafft:
    input:
        fasta=config["sample_sequences"]
    output:
        alignment=RESULT_DIR / "mafft/msa.aln"
    log:
        stderr_log=RESULT_DIR / "logs/msa/mafft/stderr.log"
    conda:
        "../envs/msa_env.yaml"
    threads: workflow.cores
    shell:
        "mafft --auto --thread {threads} {input.fasta} > {output.alignment} 2> {log.stderr_log}"

# compute the phylogenetic tree in newick format
rule create_phy_tree_newick:
    input:
        alignment=RESULT_DIR / "mafft/msa.aln"
    output:
        out_files=multiext(
            str(RESULT_DIR / "phylogeny/iqtree/msa.phy."),
            "iqtree",
            "treefile",
            "mldist",
            "log",
        )
    log:
        stderr_log=RESULT_DIR / "logs/phylogeny/iqtree/stderr.log",
        stdout_log=RESULT_DIR / "logs/phylogeny/iqtree/stdout.log"
    params:
        prefix=str(RESULT_DIR / "phylogeny/iqtree/msa.phy"),
    conda:
        "../envs/phylogeny_env.yaml"
    threads: 4
    shell:
        "iqtree --redo -s {input.alignment} -T {threads} -pre {params.prefix} > {log.stdout_log} 2> {log.stderr_log}"

# visualise the phylogenetic tree
rule create_tree_pdf:
    input:
        newick_tree=RESULT_DIR / "phylogeny/iqtree/msa.phy.treefile"
    output:
        tree_pdf=RESULT_DIR / "phylogeny/visualization/phylogenetic_tree.pdf"
    log:
        stderr_log=RESULT_DIR / "logs/phylogeny/ete3/stderr.log"
    conda:
        "../envs/phylogeny_vis_env.yaml"
    threads: 1
    shell:
        "export QT_QPA_PLATFORM='offscreen' && ete3 view -t {input.newick_tree} --image {output.tree_pdf} 2> {log.stderr_log}"
