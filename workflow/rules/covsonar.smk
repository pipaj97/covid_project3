# create the covsonar db used for matching later
rule create_covsonar_db:
    input:
        fasta=config["sample_sequences"]
    output:
        db=RESULT_DIR / "db/sonar_db"
    conda:
        config["covsonar_repo"] + "/sonar.env.yml"
    params:
        covsonar_repo=config["covsonar_repo"]
    threads: workflow.cores
    shell:
        "{params.covsonar_repo}/sonar.py add -f {input.fasta} --db {output.db} --cpus {threads}"

# add the pango lineages to the db
rule covsonar_update_lineage:
    input:
        db=RESULT_DIR / "db/sonar_db",
        pango_report=RESULT_DIR / "pangolin/lineage_report.csv"
    output:
        touch(str(RESULT_DIR / "covsonar/update_lineage.done"))
    conda:
        config["covsonar_repo"] + "/sonar.env.yml"
    params:
        covsonar_repo=config["covsonar_repo"],
        fields="accession=taxon lineage=lineage"
    threads: 4
    shell:
        "{params.covsonar_repo}/sonar.py update --db {input.db} --cpus {threads} --csv {input.pango_report} --fields {params.fields}"

# query the sequences to find all mutations compared to the wuhan reference (NC_045512.2).
rule covsonar_match:
    input:
        db=RESULT_DIR / "db/sonar_db",
        update_lineage_done=RESULT_DIR / "covsonar/update_lineage.done"
    output:
        tsv=RESULT_DIR / "covsonar/matched_seqs.tsv"
    conda:
        config["covsonar_repo"] + "/sonar.env.yml"
    params:
        covsonar_repo=config["covsonar_repo"]
    threads: 4
    shell:
        "{params.covsonar_repo}/sonar.py match --db {input.db} --cpus {threads} --tsv > {output.tsv}"