# compute risk score and print the most concerning sequence
rule compute_risk_score:
    input:
        covsonar_file = RESULT_DIR / "covsonar/matched_seqs.tsv",
        escape_scores = config["escape_scores"]
    output:
        all_risk_scores = RESULT_DIR / 'risk_score/risk_scores.csv',
        VOIs = RESULT_DIR / 'risk_score/vois.csv'
    params:
        vocs = config["risk_score_params"]["vocs"],
        mode = config["risk_score_params"]["mode"],
        weights = config["risk_score_params"]["weights"],
        cutoff = config["risk_score_params"]["cutoff"]
    conda:
        "../envs/risk_score_env.yaml"
    threads: 1
    script:
        "../scripts/predict_risk_score.py"