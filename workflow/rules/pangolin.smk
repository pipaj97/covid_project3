# assing pango lineage to the sequences
rule pangolin_assign:
    input:
        fasta=config["sample_sequences"]
    output:
        lineage_report=RESULT_DIR / "pangolin/lineage_report.csv"
    conda:
        "../envs/pangolin_env.yaml"
    params:
        out_dir= str(RESULT_DIR / "pangolin")
    threads: workflow.cores
    shell:
        "pangolin {input.fasta} -t {threads} -o {params.out_dir}"
