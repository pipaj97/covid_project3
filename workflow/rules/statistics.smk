# count mutations for plotting
rule count_mutations:
    input:
        tsv=RESULT_DIR / "covsonar/matched_seqs.tsv",
        samples=config["sample_sequences"]
    output:
        df=RESULT_DIR / "statistics/stats.tsv"
    conda:
        "../envs/count_mutations_env.yaml"
    threads: 1
    script:
        "../scripts/count_mutations.py"

# plot the counted mutations
rule plot_mutations:
    input:
        df=RESULT_DIR / "statistics/stats.tsv"
    output:
        mut_plot = RESULT_DIR / "statistics/plots/mutations.png",
        lineage_plot = RESULT_DIR / "statistics/plots/lineage.png",
        num_N_plot = RESULT_DIR / "statistics/plots/num_N.png",
        snp_indel_plot = RESULT_DIR / "statistics/plots/snp_indels.png",
        snp_indel_spike_plot = RESULT_DIR / "statistics/plots/snp_indels_spike.png"
    conda:
        "../envs/plot_mutations_env.yaml"
    threads: 1
    script:
        "../scripts/plot_mutations.py"

# compute pairwise alignment of all sequences to get the pairwise sequence identities
rule get_sequence_identity:
    input:
        samples=config["sample_sequences"]
    output:
        seq_identities= RESULT_DIR / 'statistics/clustering/seq_identity.csv'
    conda:
        "../envs/seq_identities_env.yaml"
    threads: 1
    script:
        "../scripts/get_seq_identity.py"

# PCA and kmeans
rule project_and_cluster:
    input:
        df=RESULT_DIR / "statistics/stats.tsv",
        seq_identities= RESULT_DIR / 'statistics/clustering/seq_identity.csv',
        samples=config["sample_sequences"]
    output:
        PCA_plot = RESULT_DIR / "statistics/clustering/PCA.png",
        kmeans_plot = RESULT_DIR / "statistics/clustering/kmeans.png",
        kmeans_PCA_plot = RESULT_DIR / "statistics/clustering/kmeans_PCA.png"
    conda:
        "../envs/clustering_env.yaml"
    threads: 1
    script:
        "../scripts/project_and_cluster.py"