import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from collections import Counter

df = pd.read_csv(snakemake.input[0], sep="\t")

labels = df["accession"]
sort = list(map(int, [x[1] for x in (x.split('q') for x in labels)]))
_, labels = zip(*sorted(zip(sort, labels)))

# Number of mutations in the sequences / in the spike protein
num_mut = df["num_mut"]
_, num_mut = zip(*sorted(zip(sort, num_mut)))
num_mut_in_spike = df["num_mut_in_spike"]
_, num_mut_in_spike = zip(*sorted(zip(sort, num_mut_in_spike)))

fig, ax = plt.subplots()

ax.bar(labels, 
       num_mut_in_spike, 
       label='Mutations in the Spike protein', 
       color='red')
ax.bar(labels, 
       (pd.Series(num_mut)-pd.Series(num_mut_in_spike)), 
       bottom=num_mut_in_spike, 
       color='black')

ax.set_ylabel('Mutations')
ax.set_title('Mutations in the SARS-CoV-2 sequences')
ax.legend()

plt.xticks(rotation=90)
fig.set_figheight(5)
fig.set_figwidth(15)
plt.savefig(snakemake.output[0])


# Lineages assigned by pangolin
fig, ax = plt.subplots()

lin_dict = Counter(df["lineage"])
lineage = lin_dict.keys()
y = np.arange(len(lineage))
count = lin_dict.values()

ax.barh(y, count, align='center', color='grey')
ax.set_yticks(y, labels=lineage)
ax.invert_yaxis()
ax.xaxis.get_major_locator().set_params(integer=True)
ax.set_xlabel('Number of sequences per lineage')
ax.set_title('Lineages assigned by pangolin')

fig.set_figheight(5)
fig.set_figwidth(8)
plt.savefig(snakemake.output[1])


# Number of ambiguous letters per sequence
num_N = df["num_N"]
_, num_N = zip(*sorted(zip(sort, num_N)))

fig, ax = plt.subplots()

ax.bar(labels, num_N, color='grey')
ax.set_ylabel('Ambigious letters (N)')
ax.set_title('Number of ambigious letters per sequence')

plt.xticks(rotation=90)
fig.set_figheight(5)
fig.set_figwidth(15)
plt.savefig(snakemake.output[2])


# Number of mutations (SNPs, INDELs)
num_snp = df["num_snp"]
_, num_snp = zip(*sorted(zip(sort, num_snp)))
num_del = df["num_del"]
_, num_del = zip(*sorted(zip(sort, num_del)))
num_in = df["num_in"]
_, num_in = zip(*sorted(zip(sort, num_in)))
    
fig, ax = plt.subplots()

ax.bar(labels, 
       num_snp, 
       label='SNPs', 
       color='green')
ax.bar(labels, 
       num_in, 
       bottom=(pd.Series(num_snp)+pd.Series(num_del)),
       label='Insertion',
       color='purple')
ax.bar(labels, 
       num_del, 
       bottom=num_snp,
       label='Deletion',
       color='orange')

ax.set_ylabel('Mutations')
ax.set_title('Number of Mutations')
ax.legend()

plt.xticks(rotation=90)
fig.set_figheight(5)
fig.set_figwidth(15)
plt.savefig(snakemake.output[3])


# Number of mutations in spike protein (SNPs, INDELs)
num_snp_in_spike = df["num_snp_in_spike"]
_, num_snp_in_spike = zip(*sorted(zip(sort, num_snp_in_spike)))
num_del_in_spike = df["num_del_in_spike"]
_, num_del_in_spike = zip(*sorted(zip(sort, num_del_in_spike)))
num_in_in_spike = df["num_in_in_spike"]
_, num_in_in_spike = zip(*sorted(zip(sort, num_in_in_spike)))
    
fig, ax = plt.subplots()

ax.bar(labels, 
       num_snp_in_spike, 
       label='SNPs', 
       color='green')
ax.bar(labels, 
       num_in_in_spike, 
       bottom=(pd.Series(num_snp_in_spike)+pd.Series(num_del_in_spike)),
       label='Insertion',
       color='purple')
ax.bar(labels, 
       num_del_in_spike, 
       bottom=num_snp_in_spike,
       label='Deletion',
       color='orange')

ax.set_ylabel('Mutations in Spike protein')
ax.set_title('Number of Mutations in the Spike protein')
ax.legend()

plt.xticks(rotation=90)
fig.set_figheight(5)
fig.set_figwidth(15)
plt.savefig(snakemake.output[4])