import pandas as pd
from Bio import SeqIO


df = pd.read_csv(snakemake.input[0], sep="\t")
df = df.dropna(axis=1, how="all")


# position from 21563..25384 source: https://www.ncbi.nlm.nih.gov/gene/43740568
num_mut = []
num_mut_in_spike = []

for index, row in df.iterrows():
    mutations = row["dna_profile"].split(" ")
    
    muts_in_spike = 0
    for mutation in mutations:
        if mutation.startswith("del:"):
            pos = int(mutation.split(":")[1])
        else:
            try:
                pos = int(mutation[1:-1])
            except:
                pos = int(mutation[1:-2])
    
        if ((pos >= 21563) & (pos <= 25384)):
            muts_in_spike += 1
    
    num_mut.append(len(mutations))
    num_mut_in_spike.append(muts_in_spike)

df["num_mut"] = num_mut
df["num_mut_in_spike"] = num_mut_in_spike


# Number of mutations (SNPs, INDELs)
num_snp, num_snp_in_spike = [], []
num_in, num_in_in_spike = [], []
num_del, num_del_in_spike = [], []

for index, row in df.iterrows():
    mutations = row["dna_profile"].split(" ")
    
    snp, ins, dels = 0, 0, 0
    snp_spike, in_spike, del_spike = 0, 0, 0
    for mutation in mutations:
        if mutation.startswith("del:"):
            pos = int(mutation.split(":")[1])
            dels += 1
            if ((pos >= 21563) & (pos <= 25384)):
                del_spike += 1
        else:
            try:
                pos = int(mutation[1:-1])
                snp += 1
                if ((pos >= 21563) & (pos <= 25384)):
                    snp_spike += 1
            except:
                pos = int(mutation[1:-2])
                ins += 1
                if ((pos >= 21563) & (pos <= 25384)):
                    in_spike += 1
    
    num_snp.append(snp)
    num_snp_in_spike.append(snp_spike)
    
    num_in.append(ins)
    num_in_in_spike.append(in_spike)
    
    num_del.append(dels)
    num_del_in_spike.append(del_spike)
    
df["num_snp"], df["num_snp_in_spike"] = num_snp, num_snp_in_spike
df["num_in"], df["num_in_in_spike"] = num_in, num_in_in_spike
df["num_del"], df["num_del_in_spike"] = num_del, num_del_in_spike


# Number of ambiguous letters per sequence
labels_N = []
num_N = []

for sequence in SeqIO.parse(snakemake.input[1], "fasta"):
    labels_N.append(sequence.id)
    num_N.append(sequence.seq.count('N'))

labels_N, num_N = zip(*sorted(zip(labels_N, num_N)))
df["num_N"] = num_N


df.to_csv(snakemake.output[0], sep="\t")