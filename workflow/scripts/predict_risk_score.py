import pandas as pd
from typing import Optional, List

def predict_risk_score(
    covsonar_file: str,
    escape_scores: str,
    mode: Optional[str] = 'auto',
    vocs: Optional[List] = None,
    cutoff: Optional[float] = None,
    weights: Optional[List] = [1, 0, 0],
):
    r"""
    The function computes risk scores for the SARS-CoV-2 sequences of a given covSonar output.
    This output includes lineages which were classified with pangolin.
    
    The risk score is calculated through the escape scores of the mutations in the spike protein
    of the Bloom lab (https://github.com/jbloomlab/SARS2_RBD_Ab_escape_maps).
    Deletions and mutations with no escape scores can be included in the calculations
    Args:
        covsonar_file (str): covSonar output generated with the pipeline
        escape_scores (str): csv with escape scores 
                             (https://github.com/jbloomlab/SARS2_RBD_Ab_escape_maps/blob/main/processed_data/escape_data_mutation.csv)
        mode (str, optional): 'auto' defines a cutoff, 'specific' takes cutoff from user (default: :obj:`auto`)
        vocs (list), optional: list with variances of concern, needed in 'auto' mode (default: :obj:None)
        cutoff (float, optional): sequences with risk scores >= cutoff are saved in voi.csv, 
                                  needed in 'specific' mode (default: :obj:None)
        weight (list, optional): weighting for mutations with escape score, without escape score, deletions (default: :obj:[1, 0, 0])
    """
    if mode == 'auto':
        vocs = vocs.split(", ")
    weights = weights.split(", ")
    weights = [float(weight) for weight in weights]
    mutations_df = pd.read_csv(covsonar_file, sep='\t')
    
    # reduce escape_df to wildtype mutations
    escape_df = pd.read_csv(escape_scores)
    escape_df = escape_df.loc[escape_df['source'] == 'WT convalescents']
    
    results = []
    
    for index, row in mutations_df.iterrows():
        mutations = row["aa_profile"].split(" ")
        spike_mut = [x for x in mutations if x.startswith("S:")]
        seq = row["accession"]
        lineage = row["lineage"] 
        esc, no_esc, dels = 0, 0, 0
        esc_score, no_esc_score, del_score = 0, 0, 0
        for mutation in spike_mut:
            if mutation.startswith('S:del:'):
                # get deletions in spike protein
                dels += 1
            else:
                mutation = mutation.split(":")[1]
                pos = int(mutation[1:-1])
                mut = mutation[-1]
                
                # search for position and afterwards mutation
                pos_df = escape_df.loc[escape_df['site'] == pos]
                mut_df = pos_df.loc[pos_df['mutation'] == mut]

                mut_escape = mut_df['mut_escape'].mean()
                if pd.isna(mut_escape):
                    # for no escape score in the dataframe set value to 0, otherwise NaN
                    mut_escape = 0
                    no_esc += 1
                else:
                    esc += 1
                esc_score += mut_escape*weights[0]
                no_esc_score += no_esc*weights[1]
                del_score += dels*weights[2]
        risk_score = esc_score + no_esc_score + del_score
        results.append([seq, lineage, risk_score, esc, no_esc, dels, len(spike_mut)])
    
    res_df = pd.DataFrame(results)
    res_df.columns = ['sequences', 
                      'lineage', 
                      'risk_score', 
                      'muts_w/_escape_score', 
                      'muts_w/o_escape_score', 
                      'dels_in_spike', 
                      'muts_in_spike']
    res_df.to_csv(snakemake.output[0])
    
    # in auto mode lineage is min score of the found VOC
    if mode == 'auto':
        cutoff_df = res_df.loc[res_df['lineage'].isin(vocs)]
        cutoff = min(cutoff_df['risk_score'])
    
    final_df = res_df.loc[res_df['risk_score'] >= cutoff]
    final_df.to_csv(snakemake.output[1])
    
    maxi = res_df.loc[res_df['risk_score'] == max(res_df['risk_score'])]
    seq = maxi['sequences'].values[0]
    lineage = maxi['lineage'].values[0]
    score = str(maxi['risk_score'].values[0])
    
    print('The cutoff value of the risk score for VOIs is: ' + str(cutoff))
    print('The most concerning SARS-CoV-2 sample is ' + seq + ' of lineage ' + lineage + ' with the score ' +  score)
    

mode = snakemake.params[1]
if mode == "auto":
    predict_risk_score(
        covsonar_file = snakemake.input[0],
        escape_scores = snakemake.input[1],
        vocs = snakemake.params[0],
        mode = mode,
        weights = snakemake.params[2]
    )
else:
    predict_risk_score(
        covsonar_file = snakemake.input[0],
        escape_scores = snakemake.input[1],
        vocs = snakemake.params[0],
        mode = mode,
        weights = snakemake.params[2],
        cutoff = snakemake.params[3]
    )