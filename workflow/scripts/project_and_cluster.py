import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from Bio import SeqIO
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import pairwise_distances_argmin

df = pd.read_csv(snakemake.input[0], sep="\t")

identity = pd.read_csv(snakemake.input[1])

# sort to covsonar order
seq_ids = []
for sequence in SeqIO.parse(snakemake.input[2], "fasta"):
    seq_ids.append(sequence.id)

identity.columns = seq_ids
identity.index = seq_ids
identity = identity.sort_index(axis=0)
identity = identity.sort_index(axis=1)

lineage = df["lineage"]

# PCA
X = StandardScaler().fit_transform(identity)  

pca = PCA(n_components=2)
pca_out = pca.fit_transform(X)

fig, ax = plt.subplots()
sns.scatterplot(x=pca_out[:,0], y=pca_out[:,1], hue=lineage)
plt.xlabel("PCA 1")
plt.ylabel("PCA 2")
plt.title("PCA with sequence identities")
plt.savefig(snakemake.output[0])

# kmeans
kmeans = KMeans(n_clusters=5, random_state=0)
kmeans.fit(X)

k_means_cluster_centers = kmeans.cluster_centers_
k_means_labels = pairwise_distances_argmin(X, k_means_cluster_centers)

colors = ["navy", "darkgreen", "darkorange", "purple", "red"]

fig, ax = plt.subplots()

for k, col in zip(range(5), colors):
    my_members = k_means_labels == k
    cluster_center = k_means_cluster_centers[k]
    ax.plot(X[my_members, 0], X[my_members, 1], "w", markerfacecolor=col, marker="o", alpha=0.7)
    ax.plot(
        cluster_center[0],
        cluster_center[1],
        "o",
        markerfacecolor=col,
        markeredgecolor="k",
        markersize=6,
    )
ax.set_xticks(())
ax.set_yticks(())
plt.title("KMeans with the sequence identities")
plt.savefig(snakemake.output[1])

# kmeans with pca reduced data
kmeans.fit(pca_out)

k_means_cluster_centers = kmeans.cluster_centers_
k_means_labels = pairwise_distances_argmin(pca_out, k_means_cluster_centers)

colors = ["navy", "darkgreen", "darkorange", "purple", "red"]

fig, ax = plt.subplots()

for k, col in zip(range(5), colors):
    my_members = k_means_labels == k
    cluster_center = k_means_cluster_centers[k]
    ax.plot(pca_out[my_members, 0], pca_out[my_members, 1], "w", markerfacecolor=col, marker="o", alpha=0.7)
    ax.plot(
        cluster_center[0],
        cluster_center[1],
        "o",
        markerfacecolor=col,
        markeredgecolor="k",
        markersize=6,
    )
ax.set_xticks(())
ax.set_yticks(())
plt.title("KMeans with the sequence identities (PCA)")
plt.savefig(snakemake.output[2])
