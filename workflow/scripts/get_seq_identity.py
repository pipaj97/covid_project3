import numpy as np
import pandas as pd
import biotite.sequence as seq
import biotite.sequence.align as align
import biotite.sequence.io.fasta as fasta

fasta_file = fasta.FastaFile.read(snakemake.input[0])
sequences = list(fasta.get_sequences(fasta_file).values())

matrix = align.SubstitutionMatrix.std_nucleotide_matrix()
identity_mat = np.ones((len(sequences), len(sequences)))

for i in range(len(sequences)):
    for j in range(i):
        alignment = align.align_optimal(sequences[i], sequences[j], matrix, local=False)[0]
        seq_identity = align.get_sequence_identity(alignment)
        identity_mat[i,j] = seq_identity
        identity_mat[j,i] = seq_identity   
        
df = pd.DataFrame(identity_mat)
df.to_csv(snakemake.output[0], index=False)