# covid_project3



## Setup
Clone the covsonar repository on a paralell level to this folder, so that the parent folder contains the covid_project3 folder and the covsonar repository.
The covsonar repository can be found here [https://github.com/rki-mf1/covsonar](https://github.com/rki-mf1/covsonar) and can be cloned by using the following command.

```
git clone https://github.com/rki-mf1/covsonar.git
```

Additionally to the covsonar repository, the escape scores from the Bloom lab are required. They can be found here: [https://github.com/jbloomlab/SARS2_RBD_Ab_escape_maps/blob/main/processed_data/escape_data_mutation.csv](https://github.com/jbloomlab/SARS2_RBD_Ab_escape_maps/blob/main/processed_data/escape_data_mutation.csv)
During development, the whole repository was also cloned on a paralell level to this folder.
It can be necessary to adjust the paths in the config.yaml, if the folder are somewhere else.

The pipeline was tested by using the docker container build by the dockerfile in this folder.

```
docker build -t pipaj97/sars_cov_2_project3 . 
```

After building the container it should be run with mounting the parent folder of the covsonar repository 
and the project folder. It should be noted, that the escape_data_mutation.csv also needs to be mounted.

```
cd ..
docker run --rm -it --mount src="$(pwd)",target=/project_container,type=bind pipaj97/sars_cov_2_project3
```

Switch to the right directory.
```
cd project_container/covid_project3/
```

Activate the snakemake environment.
```
conda activate snakemake
```

Run the snakemake workflow.
```
snakemake --use-conda -c
```

In the config file, parameters for the risk score computations can be adjusted. The results in the repository are created using the predefined parameter values in the config.yaml