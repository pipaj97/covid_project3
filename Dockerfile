# base image
FROM continuumio/miniconda3
#FROM ubuntu:xenial

# install basic libraries and tools
# lib is new
RUN apt update && apt install -y procps wget gzip libgl1 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# configure conda channels
RUN conda config --add channels conda-forge && \
    conda config --add channels bioconda && \
    conda config --add channels default && \
    conda config --set channel_priority strict

# regular conda stuff for snakemake
RUN conda install -n base -c conda-forge mamba -y
RUN mamba create -c conda-forge -c bioconda -n snakemake snakemake -y
RUN conda clean -a